<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checks extends Model
{
    protected $table = 'checks';
    public $primaryKey = 'checkID';
}

