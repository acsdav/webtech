<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Checks;
use App\Currency;

class CheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */

    public function index()
    {
        $checks = Checks::all();
        return view('checklist')->with('checks', $checks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function create()
    {
        return view('addcheck');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'from' => 'required',
           'to' => 'required',
            'amount' => 'required',
        ]);

        $check = new Checks;
        $check->from  = $request->input('from');
        $check->to  = $request->input('to');
        $check->amount  = $request->input('amount');
        $check->currencyID  = 'HUF';
        $check->status  = 0;
        $check -> userID = $request->input('userID');

        $check->save();

        return redirect('/checks')->with('success', 'A számla felvitele megtörtént');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function show($id)
    {
        $check = Checks::find($id);
        return view('show')->with('check', $check);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $check = Checks::find($id);
        if($request->mode == 'storno'){
            $check->status =  $request->input('status');

            if( $check->status == -1){

                $check->save();
                return redirect('/checks')->with('success', 'A számla sztornózása megtörtént');
            }else if($check ->status == 1){

                $check->save();
                return redirect('/checks')->with('success', 'A számla jóváhagyása megtörtént');
            }
        }

        if($request->mode == 'modify'){
            $number = $request->input('currencyID');

            $currencies = Currency::all();
            $to_currencyID = $currencies[$number]; //USD
            $old_rate = Currency::where('currencyID', $check->currencyID)->pluck('value_in_euros'); //HUF


            $new_amount = floatval($check->amount / $old_rate[0] * $to_currencyID->value_in_euros);

            $check -> amount = $new_amount;
            $check -> currencyID = $to_currencyID->currencyID;

            $check->save();
            return redirect('/checks')->with('success', 'Az árfolyam módosítás megtörtént');
        }

       //$check->save();

    }

    public function modify(Request $request, $id){
        $check = Checks::find($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
