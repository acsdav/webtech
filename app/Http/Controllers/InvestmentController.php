<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class InvestmentController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function listInvestments(Request $request) {

        $json = file_get_contents('https://webtech-a85b1.firebaseio.com/inv_types.json');
        $json = json_decode($json, true);
        return view('investments', ['data' => $json]);
    }
}
