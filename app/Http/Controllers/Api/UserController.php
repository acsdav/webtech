<?php

namespace App\Http\Controllers\Api;

use App\Events\UserRegistered;
use App\Http\Requests\Api\User\UserChangePasswordPostRequest;
use App\Http\Requests\Api\User\UserForgotPasswordRequest;
use App\Http\Requests\Api\User\UserPostRequest;
use App\Http\Requests\Api\User\UserPutRequest;
use App\Http\Requests\Api\User\UserUpdateProfileRequest;
use App\Repositories\UserRepository;
use App\Transformers\Api\UserTransformer;
use App\User;
use Cheppers\LaravelApiGenerator\Http\Controllers\BaseResourceController;
use Cheppers\LaravelApiGenerator\Repositories\BaseRepository;
use Cheppers\LaravelApiGenerator\Transformers\Fractal;
use Cheppers\LaravelApiGenerator\Transformers\ResourceTransformerInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Password;
use League\Fractal\Serializer\JsonApiSerializer;

/**
 * Class UserController
 * @package App\Http\Controllers\Api
 *
 * @SuppressWarnings(PHPMD)
 */
class UserController extends BaseResourceController
{
    /**
     * @api                 {get} /api/user List
     * @apiGroup            User
     * @apiName             UserList
     * @apiDescription      List all User objects.
     * @apiParam            {integer} page Page number
     * @apiParam            {integer} limit Number of items per page
     * @apiParam            {string} orderby The field name to order by
     * @apiParam            {string} sortorder Ordering direction (asc|desc)
     * @apiSuccess          {object} data List of User objects.
     * @apiSuccess          {object} meta Pagination information.
     * @apiSuccess          {object} links Pagination links.
     * @apiSuccessExample   {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "data": [
     *         {
     *           "type": "user",
     *           "id": "1",
     *           "attributes": {
     *               'name': "username",
     *               'email': "user@example.com",
     *               'strain_identifier': "sample id string",
     *               'created_at': "2018-10-31 13:45:23",
     *               'updated_at': "2018-10-31 13:45:23",
     *               "roles": [
     *                  "admin"
     *               ],
     *               "permissions": [
     *                  "admin",
     *               ]
     *           }
     *       ],
     *       "meta": {
     *         "pagination": {
     *           "total": 1,
     *           "count": 1,
     *           "per_page": 50,
     *           "current_page": 1,
     *           "total_pages": 1
     *         }
     *       },
     *       "links": {
     *         "self": "http://localhost/api/user?limit=50&page=1",
     *         "first": "http://localhost/api/user?limit=50&page=1",
     *         "last": "http://localhost/api/user?limit=50&page=1"
     *       }
     *     }
     */
    /**
     * @api                 {get} /api/user/{id} Show
     * @apiGroup            User
     * @apiName             UserShow
     * @apiDescription      Show a User object.
     * @apiParam            {integer} id Entity id.
     * @apiSuccess          {object} data The User object.
     * @apiSuccessExample   {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "data":
     *         {
     *           "type": "user",
     *           "id": "1",
     *           "attributes": {
     *               'name': "username",
     *               'email': "user@example.com",
     *               'created_at': "2018-10-31 13:45:23",
     *               'updated_at': "2018-10-31 13:45:23",
     *               "roles": [
     *                  "admin"
     *               ],
     *               "permissions": [
     *                  "admin",
     *               ]
     *           }
     *       }
     *     }
     */
    /**
     * @api                 {delete} /api/user/{id} Delete
     * @apiGroup            User
     * @apiName             UserDelete
     * @apiDescription      Delete given User object.
     * @apiParam            {integer} id Entity id.
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 204 No Content
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     */
    /**
     * @api                 {post} /api/user Store
     * @apiGroup            User
     * @apiName             UserStore
     * @apiDescription      Store a new User object.
     * @apiExample   Example-Request:
     *     {
     *       "data":
     *         {
     *           "type": "user",
     *           "attributes": {
     *               'name': "username",
     *               'email': "user@example.com",
     *               "roles": [
     *                  "admin"
     *               ]
     *           }
     *       }
     *     }
     * @apiSuccess          {object} data The created user object.
     * @apiSuccessExample   {json} Success-Response:
     *     HTTP/1.1 201 Created
     *     {
     *       "data":
     *         {
     *           "type": "factory",
     *           "id": "1",
     *           "attributes": {
     *               'name': "username",
     *               'email': "user@example.com",
     *               'created_at': "2018-10-31 13:45:23",
     *               'updated_at': "2018-10-31 13:45:23",
     *               "roles": [
     *                  "admin"
     *               ],
     *               "permissions": [
     *                  "admin",
     *               ]
     *           }
     *       }
     *     }
     */
    /**
     * @api                 {patch} /api/user/{id} Update
     * @apiGroup            User
     * @apiName             UserUpdate
     * @apiDescription      Update a User object.
     * @apiParam            {integer} id Entity id.
     * @apiExample   Example-Request:
     *     {
     *       "data":
     *         {
     *           "type": "user",
     *           "id": "1",
     *           "attributes": {
     *               'name': "username",
     *               "roles": [
     *                  "admin"
     *               ]
     *           }
     *       }
     *     }
     * @apiSuccess          {object} data The User object.
     * @apiSuccessExample   {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "data":
     *         {
     *           "type": "user",
     *           "id": "1",
     *           "attributes": {
     *               'name': "username",
     *               'email': "user@example.com",
     *               'created_at': "2018-10-31 13:45:23",
     *               'updated_at': "2018-10-31 13:45:23",
     *               "roles": [
     *                  "admin"
     *               ],
     *               "permissions": [
     *                  "admin",
     *               ]
     *           }
     *       }
     *     }
     */
    private $allowedFieldsToRegister = [
      'name',
      'email',
      'password',
      'username',
      'balance'
    ];

    /** @var \App\Repositories\UserRepository  */
    private $userRepository;

    private $allowedFieldsToUpdate = [
      'name',
    ];

    /**
     * UserController constructor.
     *
     * @param $request
     */
    public function __construct(
        Request $request,
        UserRepository $userRepository
    ) {
        parent::__construct($request);
        $this->userRepository = $userRepository;
        //$this->middleware('role:user editor')->only(['store', 'update', 'delete', 'show']);
    }


    /**
     * @api {post} /api/user/register Register
     * @apiGroup User Profile
     * @apiParam {String} type Type of entity (user).
     * @apiParam {String} name Name of the user.
     * @apiParam {String} email Email address of the user.
     * @apiParam {String} password Password of the user.
     * @apiParam {String} password_confirmation Confirmation of the password.
     * @apiParamExample {json} Example-Request:
     * {
     *     "data": {
     *         "type": "user",
     *         "attributes": {
     *             "name": "asdasd",
     *             "email": "asdahsd@asd.hu",
     *             "password": "somepw",
     *             "password_confirmation": "somepw",
     *         }
     *     }
     * }
     * @apiSuccess {object} data The stored user dataset.
     * @apiSuccess {string} type Type of entity (user).
     * @apiSuccess {number} id ID of entity.
     * @apiSuccess {string} created_at When the user was created.
     * @apiSuccess {string} updated_at When the user was updated.
     * @apiSuccess {string} name Name of the user.
     * @apiError {String} email Email address of the user.
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *         "errors": [
     *             {
     *             "status": "422",
     *             "source": {
     *                 "pointer": "/data/attributes/data.attributes.name"
     *             },
     *             "title": "Invalid parameter",
     *             "detail": "The data.attributes.name field is required."
     *             }
     *         ]
     *     }
     */
    public function register(Request $request)
    {
        $requestData = $request->all();
        $data = [];
        foreach ($this->allowedFieldsToRegister as $field) {
            if (!isset($requestData['data'][$field])) {
                continue;
            }
            $data[$field] = $requestData['data'][$field];
        }

        $data['id'] = str_random();
        $data['balance'] = 100000;
        $url = "https://webtech-f4aef.firebaseio.com/users.json";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, true));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain'));
        $jsonResponse = curl_exec($ch);
        if(curl_errno($ch))
        {
            echo 'Curl error: ' . curl_error($ch);
        }
        curl_close($ch);



        return $this->getItemResponse($data, Response::HTTP_OK);
    }


    /**
     * @api {post} /api/user/change-password Change password
     * @apiGroup User Profile
     * @apiName PostApiChangePasswordUser
     * @apiDescription Change password of the user.
     * @apiParamExample {json} Example-Request:
     * {
     *     "data": {
     *         "type": "user",
     *         "attributes": {
     *             "current_password": "alma12345",
     *             "new_password": "alma123456",
     *             "new_password_confirmation": "alma123456"
     *         }
     *     }
     * }
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     * {
     *     "data": {
     *         "type": "user",
     *         "id": "1",
     *         "attributes": {
     *             "created_at": "2018-04-09 16:17:06",
     *             "updated_at": "2018-04-10 10:30:17",
     *             "name": "admin",
     *             "email": "adminuser@example.com"
     *         }
     *     }
     * }
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 403 Forbidden
     * {
     *     "errors": [
     *         {
     *             "status": "403",
     *             "source": {
     *                 "pointer": ""
     *             },
     *             "title": "Nincs hozzáférése a funkcióhoz.",
     *             "detail": ""
     *         }
     *     ]
     * }
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     * {
     *     "errors": [
     *         {
     *             "status": "422",
     *             "source": {
     *                 "pointer": ""
     *             },
     *             "title": "A jelenlegi jelszó nem megfelelő",
     *             "detail": ""
     *         }
     *     ]
     * }
     */
    /**
     * @param UserChangePasswordPostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(UserChangePasswordPostRequest $request)
    {
        $requestData = $request->all();
        $values = $requestData['data']['attributes'];
        /** @var User $user */
        $user = auth()->user();

        if (!$user) {
            abort(Response::HTTP_FORBIDDEN, 'Nincs hozzáférése a funkcióhoz.');
        }

        if (! $this->validatePasswordFields($user, $values)) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'A jelenlegi jelszó nem megfelelő');
        }
        $modifiedData['password'] = $values['new_password'];

        $modifiedUser = $this->repository->updateUser($user->id, $modifiedData);
        return $this->getItemResponse($modifiedUser, Response::HTTP_OK);
    }

    /**
     * @api {patch} /api/user/update-profile Update profile
     * @apiGroup User Profile
     * @apiName PatchApiUpdateProfile
     * @apiDescription Update profile data of the user.
     * @apiParamExample {json} Example-Request:
     * {
     *     "data": {
     *         "type": "user",
     *         "attributes": {
     *             "name": "New username",
     *         }
     *     }
     * }
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     * {
     *     "data": {
     *         "type": "user",
     *         "id": "1",
     *         "attributes": {
     *             "created_at": "2018-04-09 16:17:06",
     *             "updated_at": "2018-04-10 10:30:17",
     *             "name": "New username",
     *             "email": "adminuser@example.com"
     *         }
     *     }
     * }
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 403 Forbidden
     * {
     *     "errors": [
     *         {
     *             "status": "403",
     *             "source": {
     *                 "pointer": ""
     *             },
     *             "title": "Nincs hozzáférése a funkcióhoz.",
     *             "detail": ""
     *         }
     *     ]
     * }
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     * {
     *     "errors": [
     *         {
     *             "status": "422",
     *             "source": {
     *                 "pointer": ""
     *             },
     *             "title": "Felhasználónév már létezik",
     *             "detail": ""
     *         }
     *     ]
     * }
     */
    /**
     * @param UserUpdateProfileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(UserUpdateProfileRequest $request)
    {
        $requestData = $request->all();
        $values = [];
        foreach ($this->allowedFieldsToUpdate as $field) {
            if (!isset($requestData['data']['attributes'][$field])) {
                continue;
            }
            $values[$field] = $requestData['data']['attributes'][$field];
        }
        /** @var User $user */
        $user = auth()->user();

        if (!$user) {
            abort(Response::HTTP_FORBIDDEN, 'Nincs hozzáférése a funkcióhoz.');
        }

        $modifiedUser = $this->repository->updateUser($user->id, $values);
        return $this->getItemResponse($modifiedUser, Response::HTTP_OK);
    }
    /**
     * @api {post} /api/user/remove Delete user
     * @apiGroup User Profile
     * @apiName ApiAuthRemoveUser
     * @apiDescription Delete logged in user with all related content.
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 204 No Content
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 403 Forbidden
     * {
     *     "errors": [
     *         {
     *             "status": "403",
     *             "source": {
     *                 "pointer": ""
     *             },
     *             "title": "Nincs hozzáférése a funkcióhoz.",
     *             "detail": ""
     *         }
     *     ]
     * }
     */
    public function remove(Request $request)
    {

        $requestData = $request->all();

        $url = "https://webtech-f4aef.firebaseio.com/users/" . $requestData['userId'] .  "/.json";
        print_r($requestData['userId']);
        $delete = $this->curl_del($url);
        return $this->getNoContentResponse();
    }

    public function curl_del($path)
    {
        $url = $path;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $result;
    }


    /**
     * @api {post} /api/user/forgotpassword Forgot password
     * @apiGroup User Profile
     * @apiName UserForgotPassword
     * @apiDescription Triggers password reminder email.
     * @apiParamExample {json} Example-Request:
     * {
     *     "data": {
     *         "type": "user",
     *         "attributes": {
     *             "email": "user@example.com",
     *         }
     *     }
     * }
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 204 No content
     */
    public function forgotPassword(UserForgotPasswordRequest $request)
    {
        $input = $request->input();
        Password::broker()->sendResetLink(['email' => $input['data']['attributes']['email']]);
        return $this->getNoContentResponse();
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $requestData = $request->all();
        $requestData = $requestData['data'];

        $json = file_get_contents('https://webtech-f4aef.firebaseio.com/users.json');
        $json = json_decode($json, true);
        $usr = [];
        foreach ($json as $id =>$user)
        {
            //$usr = json_decode($user, true);
            if(isset($user['username']) && isset($user['password'])) {
                if($user['username'] == $requestData['username'] && $user['password'] == $requestData['password']) {
                    $usr = $user;
                }
            }
        }
        if (!$usr) {
            return $this->jsonResponseUser(
                'Helytelen email cím vagy jelszó',
                Response::HTTP_UNAUTHORIZED
            );
        }
        setcookie("isLogged_" . $usr['username'], "true", time() + (86400 * 30), "/"); // 86400 = 1 day

        return response()
        ->json([
                'message' => 'Successfully login',
                'id' => $usr['id'],
                'name' => $usr['username']
            ], Response::HTTP_OK
        );
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $user = auth()->user();
        $this->guard()->logout();
        return response()
          ->json(['message' => 'Successfully logged out'])
          ->withCookie(Cookie::forget('token'));
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function mydata()
    {
        $fractal = new Fractal(new JsonApiSerializer());
        return response()->json(
            $fractal->item(
                $this->guard()->user(),
                new UserTransformer(),
                'user'
            )
        );
    }

    public function store(UserPostRequest $request)
    {
        $data = $request->input('data');
        $model = $this->getRepository()->createUser($data['attributes']);
        return $this->getItemResponse($model, Response::HTTP_CREATED);
    }

    public function update($id, UserPutRequest $request)
    {
        $input = $request->input();
        $values = $input['data']['attributes'];

        $user = auth()->user();
        if (!$user) {
            return $this->jsonResponseUser(
                'Nincs hozzáférése a funkcióhoz.',
                Response::HTTP_FORBIDDEN
            );
        }

        if (isset($values['password']) &&
            isset($values['password_confirmation']) &&
            $values['password'] != $values['password_confirmation']
        ) {
            return $this->jsonResponseUser(
                'A két jelszó nem egyezik!',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $modifiedUser = $this->repository->updateUser($id, $values);
        return $this->getItemResponse($modifiedUser, Response::HTTP_OK);
    }

    /**
     * Return token if datas is valid
     * @param $credentials
     * @param $user
     *
     * @return mixed
     */
    private function getLoginToken($credentials, $user)
    {
        $token = $this
          ->guard()
          ->attempt($credentials);

        if (!$token && md5($credentials['password']) === $user['old_password']) {
            $this->userRepository->updateUser(
                $user['id'],
                ['old_password' => '', 'password' => $credentials['password']]
            );
            $token = $this
              ->guard()
              ->attempt($credentials);
        }
        return $token;
    }

    private function jsonResponseUser($detail, $responseCode)
    {
        return response()->json(['errors' => [
          [
            'status' => $responseCode,
            'detail' => $detail,
          ]
        ]], $responseCode);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
          'access_token' => $token,
          'token_type' => 'bearer',
          'expires_in' => $this->guard()->factory()->getTTL(),
        ]);
    }

    protected function validatePasswordFields(User $user, $values)
    {
        $userCredentials = [
            'email' => $user->email,
            'password' => $values['current_password'] ?? '',
        ];

        return auth()->validate($userCredentials);
    }

    protected function getTransformer(): ResourceTransformerInterface
    {
        return new UserTransformer();
    }

    protected function getRepository(): BaseRepository
    {
        return new UserRepository();
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }

    protected function getFilterInfo(Request $request)
    {
        return [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
        ];
    }
}
