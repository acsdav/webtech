<?php

namespace App\Http\Controllers\Api;

use App\Events\UserRegistered;
use App\Repositories\UserRepository;
use App\Transformers\Api\UserTransformer;
use App\User;
use Cheppers\LaravelApiGenerator\Http\Controllers\BaseResourceController;
use Cheppers\LaravelApiGenerator\Repositories\BaseRepository;
use Cheppers\LaravelApiGenerator\Transformers\Fractal;
use Cheppers\LaravelApiGenerator\Transformers\ResourceTransformerInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Password;
use League\Fractal\Serializer\JsonApiSerializer;


class PurchaseController extends BaseResourceController
{
    private $allowedFieldsToPurchase = [

      'userID',
      'invID',
      'quantity',
    ];

    public function purchase(Request $request)
    {
        $requestData = $request->all();
        $data = [];
        foreach ($this->allowedFieldsToPurchase as $field) {
            if (!isset($requestData['data'][$field])) {
                continue;
            }
            $data[$field] = $requestData['data'][$field];
        }
        $url = "https://webtech-ded1a.firebaseio.com/bought_invs.json";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, true));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain'));
        $jsonResponse = curl_exec($ch);
        if(curl_errno($ch))
        {
            echo 'Curl error: ' . curl_error($ch);
        }
        curl_close($ch);

        return response()->json($data, Response::HTTP_OK);
    }
    
    protected function getTransformer(): ResourceTransformerInterface
    {
        return new UserTransformer();
    }

    protected function getRepository(): BaseRepository
    {
        return new UserRepository();
    }
}