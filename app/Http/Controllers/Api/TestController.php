<?php
/**
 * Created by PhpStorm.
 * User: mozsi
 * Date: 2018.10.18.
 * Time: 13:34
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function testendpoint()
    {
        return response()->json([
            'data' => [
                'id' => 1,
                'type' => 'test',
                'attributes' => [
                    'ping' => 'pong',
                ],
            ],
        ]);
    }
}
