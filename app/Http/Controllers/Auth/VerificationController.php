<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

class VerificationController extends Controller
{
    /**
     * @var \App\Repositories\UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function verify($token)
    {
        $verifySuccess = $this->userRepository->verifyEmail($token);
        return view('auth.verification', ['verified' => $verifySuccess]);
    }
}
