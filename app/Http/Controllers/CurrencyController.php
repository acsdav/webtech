<?php

namespace App\Http\Controllers;

use App\Checks;
use App\Currency;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Route;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $currencies = Currency::all();
        return view('exchange')->with('currencies', $currencies);
    }
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */

    public function getCurrencies(){
        $id = Route::current()->parameter('id');
        $currencies = Currency::all();

        return view('modifycheck')->with('currencies', $currencies)->with('id',$id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://api.exchangeratesapi.io/',
        ]);
        $response = $client->request('GET', 'latest', [
            'query' => [
                'base' => 'EUR',
            ]
        ]);
        if ($response->getStatusCode() == 200) {
            $body = $response->getBody();
            $arr_body = json_decode($body);
        }

        $result = $this->objectToArray($arr_body->rates);

        $keys = (array_keys($result));
        for ($i = 0, $size = count($result); $i < $size; ++$i) {
            $currency = new Currency;
            $currency->currencyID = $keys[$i];
            $currency->value_in_euros = $result[$keys[$i]];
            $currency->save();
        }

        return redirect('/exchange')->with('success', 'Árfolyamok feltöltve');

    }

    function objectToArray($data){
        if(is_object($data)){
            $d = get_object_vars($data);
        }
        return $d;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateCurrency(Request $request){
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://api.exchangeratesapi.io/',
        ]);
        $response = $client->request('GET', 'latest', [
            'query' => [
                'base' => 'EUR',
            ]
        ]);
        if ($response->getStatusCode() == 200) {
            $body = $response->getBody();
            $arr_body = json_decode($body);
        }

        $result = $this->objectToArray($arr_body->rates);

        $keys = (array_keys($result));

        Currency::query()->delete();

        for ($i = 0, $size = count($result); $i < $size; ++$i) {
            $currency = new Currency;
            $currency->currencyID = $keys[$i];
            $currency->value_in_euros = $result[$keys[$i]];
            $currency->save();
        }

        return redirect('/exchange')->with('success', 'Árfolyamok frissítve');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
