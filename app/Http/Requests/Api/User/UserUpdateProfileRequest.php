<?php

namespace App\Http\Requests\Api\User;

use Cheppers\LaravelApiGenerator\Http\Requests\Api\PostRequest;

class UserUpdateProfileRequest extends PostRequest
{
    protected function addRules(): array
    {
        return parent::addRules() + [
            'data.attributes.name' => 'string|max:50',
          ];
    }
}
