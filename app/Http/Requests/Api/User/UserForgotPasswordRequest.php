<?php


namespace App\Http\Requests\Api\User;

use Cheppers\LaravelApiGenerator\Http\Requests\Api\PostRequest;

class UserForgotPasswordRequest extends PostRequest
{
    protected function addRules(): array
    {
        return parent::addRules() + ['data.attributes.email' => 'required|email'];
    }
}
