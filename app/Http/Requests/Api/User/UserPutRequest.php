<?php

namespace App\Http\Requests\Api\User;

use Cheppers\LaravelApiGenerator\Http\Requests\Api\PutRequest;

class UserPutRequest extends PutRequest
{
    protected function addRules(): array
    {
        return parent::addRules() + [];
    }
}
