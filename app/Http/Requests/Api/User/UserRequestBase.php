<?php

namespace App\Http\Requests\Api\User;

use App\User;
use Carbon\Carbon;

class UserRequestBase
{
    public static function addRules()
    {
        return [
            'data.attributes.name' => 'required|string|max:50',
            'data.attributes.email' => 'required|string|email|max:255',
            'data.attributes.password' => 'required|string|min:6|confirmed',
        ];
    }
}
