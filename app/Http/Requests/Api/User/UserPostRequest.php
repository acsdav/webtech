<?php

namespace App\Http\Requests\Api\User;

use Cheppers\LaravelApiGenerator\Http\Requests\Api\PostRequest;

class UserPostRequest extends PostRequest
{
    protected function addRules(): array
    {
        return parent::addRules() + UserRequestBase::addRules();
    }
}
