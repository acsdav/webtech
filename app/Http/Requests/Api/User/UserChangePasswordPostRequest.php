<?php

namespace App\Http\Requests\Api\User;

use Cheppers\LaravelApiGenerator\Http\Requests\Api\PostRequest;

class UserChangePasswordPostRequest extends PostRequest
{
    protected function addRules(): array
    {
        return parent::addRules() + [
            'data.attributes.current_password' => 'required',
            'data.attributes.new_password' => 'required_with:current_password',
            'data.attributes.new_password_confirmation' => 'same:data.attributes.new_password',
        ];
    }
}
