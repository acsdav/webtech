<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuthFacade;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JWTAuth extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            JWTAuthFacade::parseToken()->authenticate();
        } catch (TokenInvalidException $exception) {
            return response()->json(['status' => 'Token is invalid'], Response::HTTP_UNAUTHORIZED);
        } catch (TokenExpiredException $exception) {
            return response()->json(['status' => 'Token is expired'], Response::HTTP_UNAUTHORIZED);
        } catch (JWTException $exception) {
            abort(Response::HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }
}
