<?php

namespace App\Transformers\Api;

use App\User;
use Cheppers\LaravelApiGenerator\Transformers\BaseResourceTransformer;
use Cheppers\LaravelApiGenerator\Transformers\ResourceTransformerInterface;

class UserTransformer extends BaseResourceTransformer implements ResourceTransformerInterface
{
    public static function getResourceKey()
    {
        return 'user';
    }

    public function transform( $user)
    {

        return [
            'id' => $user['id'],
            'name' => $user['name'],
            'email' => $user['email'],
        ];
    }
}
