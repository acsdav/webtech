<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Mail\Mailers\UserMailer;

class UserRegisterListener
{
    /**
     * @var $mailer \App\Mail\Mailers\UserMailer
     */
    private $mailer;

    /**
     * UserRegisterListener constructor.
     *
     * @param \App\Mail\Mailers\UserMailer $mailer
     */
    public function __construct(UserMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $this->mailer->sendEmailConfirmationTo($event->user)->send();
    }
}
