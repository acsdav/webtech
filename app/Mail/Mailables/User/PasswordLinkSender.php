<?php

namespace App\Mail\Mailables\User;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordLinkSender extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    private $token;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.users.password')
            ->with(['user' => $this->user, 'token' => $this->token])
            ->to($this->user->email)
            ->subject(config('mail.subject.password'))
            ->from(config('mail.from.address'), config('mail.from.name'));
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}
