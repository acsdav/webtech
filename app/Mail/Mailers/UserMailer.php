<?php
namespace App\Mail\Mailers;

use App\Mail\Mailables\User\PasswordLinkSender;
use App\Mail\Mailables\User\TextBasedForecastStoredSender;
use App\Mail\Mailables\User\UserBecameAdminSender;
use App\Mail\Mailables\User\VerificationSender;
use App\Models\TextBasedForecast;
use App\User;

class UserMailer extends MailerBase
{
    public function sendEmailConfirmationTo(User $user)
    {
        $this->message = new VerificationSender($user);

        return $this;
    }

    public function sendPasswordLinkTo(User $user, $token)
    {
        $this->message = new PasswordLinkSender($user, $token);

        return $this;
    }

    public function sendBecameAdminTo(User $user)
    {
        $this->message = new UserBecameAdminSender($user);

        return $this;
    }

    public function sendTextBasedForecast(TextBasedForecast $textBasedForecast)
    {
        $this->message = new TextBasedForecastStoredSender($textBasedForecast);

        return $this;
    }
}
