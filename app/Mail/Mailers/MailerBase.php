<?php

namespace App\Mail\Mailers;

use Illuminate\Support\Facades\Mail;

abstract class MailerBase
{
    protected $message;

    public function send()
    {
        Mail::send($this->message);
    }

    public function queue()
    {
        Mail::queue($this->message);
    }

    public function later($delay)
    {
        Mail::later($delay, $this->message);
    }
}
