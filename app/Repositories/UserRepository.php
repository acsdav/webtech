<?php

namespace App\Repositories;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Cheppers\LaravelApiGenerator\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    protected $allowedOrders = [
        'id',
        'created_at',
        'id',
        'email',
        'name',
    ];
    protected $allowedFilters = [
        'email',
        'name',
    ];

    public function getModelClass()
    {
        return User::class;
    }

    public function isUserActive(String $email)
    {
        /**
         * @var Builder $query
         */
        $query = $this->model
            ->where('email', $email)
            ->whereNotNull('email_verified_at');
        $activeUsersWithEmail = $query->count();

        return $activeUsersWithEmail === 1;
    }

    public function createUser($data)
    {
        $data['password'] = self::hash($data['password']);

        /**
         * @var User $user
         */
        $user = $this->store($data);
        if (isset($data['roles'])) {
            $user->syncRoles($data['roles']);
        }
        return $user;
    }

    public function updateUser($id, $data)
    {
        if (isset($data['password'])) {
            $data['password'] = self::hash($data['password']);
        }

        /**
         * @var User $user
         */
        $user = $this->update($id, $data);
        if (isset($data['roles'])) {
            $user->syncRoles($data['roles']);
        }
        return $user;
    }

    public static function hash($value)
    {
        return \Hash::make($value);
    }

    public function getUserByEmail(String $email)
    {
        return $this->model->where('email', $email)->first();
    }

    public function getModel()
    {
        return $this->model;
    }

    protected function filterQuery($filterName, $filterValue, $query)
    {
        switch ($filterName) {
            case 'email':
                return $query->where('email', 'LIKE', '%' . $filterValue . '%');
            case 'name':
                return $query->where('name', 'LIKE', '%' . $filterValue . '%');
            default:
                return $query;
        }
    }
    public function delete($id)
    {
        /** @var User $user */
        $user = $this->model->findOrFail($id);
        return $user->delete();
    }

    public function verifyEmail($token)
    {
        $user = $this->model->where('verification_token', '=', $token)->first();

        if (!empty($user)) {
            $user->verification_token = null;
            $user->email_verified_at = Carbon::now();
            $user->save();
            return true;
        }
        return false;
    }
}
