@extends('layouts.main')
    @section('title', 'Project')

    @section('content')

        <div class="">
            <div class="loginForm">
                <h3>Befektetés vásárlás</h3><br>
                <div class="post-route" style="display: none">{{ route('api.purchase') }}</div>
                <form class="purchase-form" enctype='application/json'>
                    @csrf
                    <label for="invID">Befektetés ID <div class="alert-star">*</div></label>
                    <select id="invID" name="invID">
                        <option value=1>1</option>
                        <option value=2>2</option>
                        <option value=3>3</option>
                    </select>

                    <label for="quantity">Mennyiség <div class="alert-star">*</div></label>
                    <input  required="required" type="text" id="quantity" name="quantity">

                    <input type="submit" class="submit-form-purchase" value="Vásárlás">
                </form>
            </div>

            <script src="{{ asset('js/purchase.js') }}"></script>

        </div>


    @stop
