@extends('layouts.main')
    @section('title', 'Project')

    @section('content')

        <div class="">
            <h3>Felhasználók listázása</h3><br>


            <br>
            @foreach ($data as $d => $user)
                <div class="user-div" id="{{ $user['id'] }}">
                    <div class="user-div-id">
                        <label>ID: </label>
                        <div>{{ $user['id'] }}</div>
                    </div>
                    <div class="user-div-username">
                        <label>Felhasználónév: </label>
                        <div>{{ $user['username'] }}</div>
                    </div>
                    <div class="user-div-name">
                        <label>Név: </label>
                        <div>{{ $user['name'] }}</div>
                    </div>
                    <div class="user-div-email">
                        <label>Email: </label>
                        <div>{{ $user['email'] }}</div>
                    </div>
                </div>
            @endforeach
{{--            {{ $data }}--}}
{{--            <div class="post-route" style="display: none">{{ route('api.register') }}</div>--}}
{{--            <form class="registration-form" enctype='application/json'>--}}
{{--                @csrf--}}
{{--                <label for="username">Felhasználó <div class="alert-star">*</div></label>--}}
{{--                <input required="required" type="text" id="username" name="username" placeholder="Felhasználóneved ...">--}}

{{--                <label for="name">Teljes név <div class="alert-star">*</div></label>--}}
{{--                <input  required="required" type="text" id="name" name="name" placeholder="Teljes név...">--}}

{{--                <label for="email">Email <div class="alert-star">*</div></label>--}}
{{--                <input required="required" type="email" id="email" name="email" placeholder="Email...">--}}

{{--                <label for="password">Jelszó <div class="alert-star">*</div></label>--}}
{{--                <input  required="required" type="password" id="password" name="password" placeholder="Jelszó...">--}}

{{--                <input type="submit" class="submit-form-registration" value="Submit">--}}
{{--            </form>--}}
        </div>


    @stop
