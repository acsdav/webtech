@extends('layouts.main')


@section('content')
    <h1>{{$check->checkID}}. számla adatai</h1>
    <div class="row">
        <small>Hozzáadva: {{$check->created_at}}</small>
    </div>
    <div class="row">
        <small>Utoljára módosítva: {{$check->updated_at}}</small>
    </div>
    <br>
    {!! Form::open(['action' => 'CheckController@store', 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('from', 'Honnan')}}
        {{Form::text('from', $check->from, ['class' =>'form-control', 'placeholder'=>'','disabled'=>true])}}

        {{Form::label('to', 'Hova')}}
        {{Form::text('to', $check->to, ['class' =>'form-control', 'placeholder'=>'','disabled'=>true])}}

        {{Form::label('amount', 'Összeg')}}
        {{Form::text('amount', $check->amount, ['class' =>'form-control', 'placeholder'=>'','disabled'=>true])}}

        {{Form::label('currency', 'Valuta')}}
        {{Form::text('currency', $check->currencyID, ['class' =>'form-control', 'disabled'=>true])}}

        {{Form::text('userID', Cookie::get('userId'), ['hidden'=>true])}}

        <label for="status">Státusz </label>
        <input required="required" type="text" id="status" name="status" value="{{$check->status}}" disabled="true">

    </div>
    {!! Form::close() !!}

    <nav>
        <ul>
            <div class="row">
                <li class="main-confirm-button"><a href="{{$check->checkID}}/confirmcheck">Jóváhagyás</a>
                <li class="main-storno-button"><a href="{{$check->checkID}}/stornocheck">Sztornózás</a>
                <li class="main-confirm-button"><a href="{{$check->checkID}}/modifycheck">Árfolyam módosítás</a>
            </div>
        </ul>
    </nav>
@endsection
