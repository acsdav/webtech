@extends('layouts.main')

@section('title', 'Project')

@section('content')

    <h3>Árfolyam váltás</h3>

    <nav>
        <ul>
            <div class="row">
    <li class="main-button" hidden="true" ><a href="/load">Feltöltés</a>
    <li class="main-button"><a href="/update">Frissítés</a>
            </div>
        </ul>
    </nav>

    <div class="form-group">

        <label for="from_curr_label">Honnan </label>
        <select id="from_curr">

            @foreach($currencies as $currency)
                <option value={{$currency['value_in_euros']}}>{{$currency['currencyID']}}</option>
            @endforeach
        </select>

        <label for="from_amount_label">Összeg </label>
        <input required="required" type="text" id="from_amount" name="from_amount">

        <label for="to_curr_label">Hova </label>
        <select id="to_curr">
            @foreach($currencies as $currency)
                <option value={{$currency['value_in_euros']}}>{{$currency['currencyID']}}</option>
            @endforeach
        </select>

        <label for="to_amount_label">Összeg </label>
        <input type="text" id="to_amount" name="to_amount" disabled="true">

        <button id="change_link">Átváltás</button>

    </div>
@endsection

