@extends('layouts.main')

@section('content')
    <h1> {{$id}}. számú számla jóváhagyása</h1>

    <h3>Biztos jóváhagyja a kiválasztott számlát?</h3>
    {!! Form::open(['action' => ['CheckController@update', $id], 'method' => 'POST']) !!}

    {{Form::hidden('mode', 'storno')}}
    {{Form::hidden('status', '1')}}
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Jóváhagyás', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}

@endsection
