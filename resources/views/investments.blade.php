@extends('layouts.main')
    @section('title', 'Project')

    @section('content')

        <div class="">
            <h3>Elérhető befektetések</h3><br>
            @foreach ($data as $d => $inv)
                <div class="user-div" id="{{ $inv['id'] }}">
                    <div class="inv-div-id">
                        <label>ID: </label>
                        <div>{{ $inv['id'] }}</div>
                    </div>
                    <div class="inv-div-name">
                        <label>Név: </label>
                        <div>{{ $inv['name'] }}</div>
                    </div>
                    <div class="inv-div-unit_price">
                        <label>Egységár: </label>
                        <div>{{ $inv['unit_price'] }}</div>
                    </div>
                    <div class="inv-div-currency">
                        <label>Pénznem: </label>
                        <div>{{ $inv['currency'] }}</div>
                    </div>
                    <div class="inv-div-rate_max">
                        <label>Hozam: </label>
                        <div>{{ $inv['rate_max'] }}%</div>
                    </div>
                </div>
            @endforeach

            <nav>
                <ul>
                    <li class="purchase-button" ><a href="/purchase">Befektetés vásárlás</a>
                </ul>
            </nav>
        </div>


    @stop
