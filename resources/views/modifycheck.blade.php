@extends('layouts.main')

@section('content')
    <h1>{{ $id }}. számla árfolyamának módosítása</h1>
    {!! Form::open(['action' => ['CheckController@update', $id ], 'method' => 'POST']) !!}
    <label for="to_curr_label">Válassza ki a kívánt árfolyamot: </label>

    {{ Form::select('currencyID', $currencies->pluck('currencyID')), ['value' =>$currencies->pluck('currencyID')]}}

    {{Form::hidden('mode', 'modify')}}

    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Átváltás', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
