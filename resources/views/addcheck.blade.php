@extends('layouts.main')

@section('content')
    {!! Form::open(['action' => 'CheckController@store', 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('from', 'Honnan')}}
        {{Form::text('from', '', ['class' =>'form-control', 'placeholder'=>''])}}

        {{Form::label('to', 'Hova')}}
        {{Form::text('to', '', ['class' =>'form-control', 'placeholder'=>''])}}

        {{Form::label('amount', 'Összeg')}}
        {{Form::text('amount', '', ['class' =>'form-control', 'placeholder'=>''])}}

        {{Form::label('currency', 'Valuta')}}
        {{Form::text('currency', 'HUF', ['class' =>'form-control', 'placeholder'=>'HUF', 'disabled'=>true, 'value'=>'HUF'])}}

        {{Form::text('userID', Cookie::get('userId'), ['hidden'=>true])}}
    </div>
    {{Form::submit('Mentés', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
