@extends('layouts.main')

@section('content')
    <h1> {{$id}}. számú számla sztornózása</h1>

    <h3>Biztos sztornózza a kiválasztott számlát?</h3>
    {!! Form::open(['action' => ['CheckController@update', $id], 'method' => 'POST']) !!}
    {{Form::hidden('mode', 'storno')}}
    {{Form::hidden('status', '-1')}}
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Sztornózás', ['class' => 'btn btn-secondary'])}}
    {!! Form::close() !!}

@endsection
