@extends('layouts.main')

@section('title', 'Új jelszó megadása')

@section('content')
    <div class="section-head">
        <h2>Új jelszó megadása</h2>
    </div>

    <form id="pwChangeForm" class="box" method="POST" action="{{ route('password.request') }}" novalidate>
        {{ csrf_field() }}

        <input type="hidden" name="token" value="{{ $token }}">

        @if ($errors->has('token'))
            <div class="alert alert-danger">
                {{ $errors->first('token') }}
            </div>
        @endif
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="form-group">
            <label for="pwChangeEmail"{!! $errors->has('email') ? ' class="text-danger"' : '' !!}>E-mail cím</label>
            <input type="text" id="pwChangeEmail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $email) }}">
        </div>

        <div class="form-group">
            <label for="pwChangePassword"{!! $errors->has('password') ? ' class="text-danger"' : '' !!}>Jelszó</label>
            <input type="password" id="pwChangePassword" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
        </div>

        <div class="form-group">
            <label for="pwChangePasswordConf"{!! $errors->has('password_confirmation') ? ' class="text-danger"' : '' !!}>Jelszó még egyszer</label>
            <input type="password" id="pwChangePasswordConf" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation">
        </div>

        <div class="row justify-content-md-center">
            <div class="col-lg-8 col-xl-6">
                <button type="submit" class="btn btn-block btn-success">Jelszó beállítása</button>
            </div>
        </div>
    </form>
@endsection
