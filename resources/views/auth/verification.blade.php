@extends('layouts.main')

@section('title', 'Megerősítés')

@section('content')
    @if ($verified)
        <p>A megerősítés sikerült.</p>
    @else
        <p>A megerősítés sikertelen!</p>
    @endif
@endsection
