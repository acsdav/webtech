@extends('layouts.main')
@section('title', 'Project')


@section('content')
    <div class="row">
        <div class="clod-md-6 col-lg-6">
        <h1>Számlák listázása</h1><br>

            <h3>Aktív számlák</h3>
        @if(count($checks) > 0)
            @foreach($checks as $check)
                @if( Cookie::get('userId') == $check['userID'] && $check['status'] == 0)
                    <div class="check-div" id="{{ $check['checkID'] }}">
                        <div class="check-div-id">
                            <a href="/checks/{{$check['checkID']}}">
                                <label>ID: </label>
                                <div>{{ $check['checkID'] }}</div>
                            </a>
                        </div>
                        <div class="check-div-from">
                            <label>Honnan: </label>
                            <div>{{ $check['from'] }}</div>
                        </div>
                        <div class="check-div-to">
                            <label>Hova: </label>
                            <div>{{ $check['to'] }}</div>
                        </div>
                        <div class="check-div-amount">
                            <label>Összeg: </label>
                            <div>{{ $check['amount'] }}</div>
                        </div>
                        <div class="check-div-currency">
                            <label>Valuta: </label>
                            <div>{{ $check['currencyID'] }}</div>
                        </div>
                    </div>
                    @endif
            @endforeach

            <h3>Inaktív számlák</h3>
                @foreach($checks as $check)
                    @if( Cookie::get('userId') == $check['userID'] && $check['status'] == -1)
                        <div class="check-div" id="{{ $check['checkID'] }}">
                            <div class="check-div-id">
                                <a href="/checks/{{$check['checkID']}}">
                                    <label>ID: </label>
                                    <div>{{ $check['checkID'] }}</div>
                                </a>
                            </div>
                            <div class="check-div-from">
                                <label>Honnan: </label>
                                <div>{{ $check['from'] }}</div>
                            </div>
                            <div class="check-div-to">
                                <label>Hova: </label>
                                <div>{{ $check['to'] }}</div>
                            </div>
                            <div class="check-div-amount">
                                <label>Összeg: </label>
                                <div>{{ $check['amount'] }}</div>
                            </div>
                            <div class="check-div-currency">
                                <label>Valuta: </label>
                                <div>{{ $check['currencyID'] }}</div>
                            </div>
                        </div>
                    @endif
                @endforeach

                <h3>Jóváhagyott számlák</h3>
                @foreach($checks as $check)
                    @if( Cookie::get('userId') == $check['userID'] && $check['status'] == 1)
                        <div class="check-div" id="{{ $check['checkID'] }}">
                            <div class="check-div-id">
                                <a href="/checks/{{$check['checkID']}}">
                                    <label>ID: </label>
                                    <div>{{ $check['checkID'] }}</div>
                                </a>
                            </div>
                            <div class="check-div-from">
                                <label>Honnan: </label>
                                <div>{{ $check['from'] }}</div>
                            </div>
                            <div class="check-div-to">
                                <label>Hova: </label>
                                <div>{{ $check['to'] }}</div>
                            </div>
                            <div class="check-div-amount">
                                <label>Összeg: </label>
                                <div>{{ $check['amount'] }}</div>
                            </div>
                            <div class="check-div-currency">
                                <label>Valuta: </label>
                                <div>{{ $check['currencyID'] }}</div>
                            </div>
                        </div>
                    @endif
                @endforeach

        @else
            <p>Önnek nincs megjeleníthető számlája.</p>
        @endif
        </div>

    </div>
    <nav>
        <ul>
            <div class="row">
                <li class="main-add-button"><a href="/addcheck">Új hozzáadása</a>
            </div>
        </ul>
    </nav>


@endsection

