@extends('layouts.main')
    @section('title', 'Project')

    @section('content')

        <div class="loginForm">
            <h3>Bejelentkezés</h3><br>
            <div class="post-route" style="display: none">{{ route('api.auth.login') }}</div>
            <form class="login-form" enctype='application/json'>
                @csrf
                <label for="username">Felhasználó </label>
                <input required="required" type="text" id="username" name="username" placeholder="Felhasználóneved ...">

                <label for="password">Jelszó</label>
                <input  required="required" type="password" id="password" name="password" placeholder="Jelszó...">

                <input type="submit" class="submit-form-login" value="Submit">
            </form>
        </div>

        <script src="{{ asset('js/login.js') }}"></script>


    @stop
