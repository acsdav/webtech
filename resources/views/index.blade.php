@extends('layouts.main')
    @section('title', 'Project')

    @section('content')
        <div id="app">
            <app-layout></app-layout>
        </div>


	<section id="pageContent">
		<main role="main">

			<article>
				<h2>Felhasználók kezelése</h2>
                <p>
                    A felhasználókezelési modul tartalmazza ezen entitás elemeinek a menedzselését.
                    A felhasználói modul funkciói a következők: Regisztráció, Bejelentkezés, Fiók törlése, Összes felhasználó listázása funkciók, Saját adat módosítása. A felhasználónak szükséges egyedi azonosító (email / username), amelyet a bejelentkezéshez kell, hogy megadja.
                </p>
            </article>
			<article>
				<h2>Számlakezelés és árfolyam váltás</h2>
                <p>Tranzakció követés modulban találhatóak meg a bejelentkezett felhasználó számlái, amikkel rendelkezni tud a későbbiekben. Lehetősége van számlát felvinni, illetve aktív számlát jóváhagyni, vagy nem kellő számlákat sztornózni.
                    Számla módosítására lehetősége van olyan módon, hogy a már meglévő számláinak árfolyamát tudja módosítani. Ilyenkor árfolyam választás után a számla egyenlege automatikusan átvált.

                    A pénzváltási modul külön modulként is megjelenik, ahol nem bejelentkezett felhasználók is tudnak két tetszőleges árfolyam között váltani.
                </p>
			</article>
			<article>
				<h2>Befektetések kezelése</h2>
<p>
    A harmadik modulban befektetések alapvető kezelésére van lehetőség. Minden user rendelkezik egy forint alapú egyenleggel, amelyből vásárolhat befektetéseket. Lesz továbbá lehetőség a meglévő befektetések listázására is, illetve azok eladására. Ez utóbbi esetben kamat is felszámításra kerül, ami negatív is lehet, attól függően, hogy milyen kamatszázalék tartomány tartozik az egyes befektetés típusokhoz.
    A befektetések különböző pénznemekben értelmezettek lehetnek. Mivel az egyenlegek forint alapúak, vásárláskor és eladáskor áfolyamszámításra is sor kerül, amely épít a második modulra.

</p>			</article>


        <script src="{{ asset('js/app.js') }}"></script>
    @stop
