@extends('layouts.main')

@section('title', 'Profilom')

@section('content')
    <h2>Profilom</h2>
    <div id="app">
        <my-profile></my-profile>
    </div>
@endsection