<!DOCTYPE html>
<html>
<head>
<title>Számlázási rendszer</title>
    <meta name="description" content="website description" />
    <meta name="keywords" content="website keywords, website keywords" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" title="style" />
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/notify.js') }}"></script>

    <script defer src="https://www.gstatic.com/firebasejs/7.14.3/firebase-app.js"></script>

    <script defer src="https://www.gstatic.com/firebasejs/7.14.3/firebase-auth.js"></script>
    <script defer src="https://www.gstatic.com/firebasejs/7.14.3/firebase-firestore.js"></script>

    <script defer src="{{ asset('js/init-firebase.js') }}"></script>

    <script src="{{ asset('js/main.js') }}"></script>

    <script src="{{ asset('js/checks.js') }}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    <header>
        <h1><a class="main-title" href="/">Számlázási rendszer</a></h1>
		<nav>
			<ul>
				<li class="main-main-button" ><a href="/">Főoldal</a>
				<li class="main-login-button" ><a href="/login">Bejelentkezés</a>
				<li class="main-registration-button" ><a href="/registration">Regisztráció</a>
				<li class="main-list-users-button" ><a href="/list-users">Userek listázása</a>
				<li class="main-profile-delete-button" >

                    <div class="delete-route" style="display: none">{{ route('api.auth.remove') }}</div>
                    <a href="">
                        Profil törlés
                    </a>
				<li class="main-logout-button" ><a href="">Kijelentkezés</a>
                <li class="main-main-button"><a href="/exchange">Árfolyam váltó</a>
                <li class="main-check-button"><a href="/checks">Számlák</a>
                <li class="main-list-investments-button" ><a href="/list-investments">Elérhető befektetések</a>
			</ul>
		</nav>
	</header>

	<section id="pageContent">
		<main role="main">
            @include('inc.messages')
         @yield('content')
		</main>

	</section>


    <footer>
		<p>Webtechnológia beadandó 2020</p>
	</footer>


{{--<script>--}}
{{--    var config = {--}}
{{--        apiKey: "AIzaSyA1Wmr9FAOm9XrGzcUu46fcy8XyfRoEITk",--}}
{{--        authDomain: "webtech-f4aef.firebaseapp.com",--}}
{{--        databaseURL: "https://webtech-f4aef.firebaseio.com",--}}
{{--        //storageBucket: "bucket.appspot.com"--}}
{{--    };--}}
{{--    firebase.initializeApp(config);--}}

{{--</script>--}}

</body>
</html>
