@extends('layouts.main')
    @section('title', 'Project')

    @section('content')

        <div class="loginForm">
            <h3>Regisztráció</h3><br>
            <div class="post-route" style="display: none">{{ route('api.register') }}</div>
            <form class="registration-form" enctype='application/json'>
                @csrf
                <label for="username">Felhasználó <div class="alert-star">*</div></label>
                <input required="required" type="text" id="username" name="username" placeholder="Felhasználóneved ...">

                <label for="name">Teljes név <div class="alert-star">*</div></label>
                <input  required="required" type="text" id="name" name="name" placeholder="Teljes név...">

                <label for="email">Email <div class="alert-star">*</div></label>
                <input required="required" type="email" id="email" name="email" placeholder="Email...">

                <label for="password">Jelszó <div class="alert-star">*</div></label>
                <input  required="required" type="password" id="password" name="password" placeholder="Jelszó...">

                <input type="submit" class="submit-form-registration" value="Submit">
            </form>
        </div>

        <script src="{{ asset('js/registration.js') }}"></script>

    @stop
