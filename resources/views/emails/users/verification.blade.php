<h1>Kedves {{$user->name}}!</h1>

<p>A regisztrációd befejezéséhez kérjük kattints az alábbi linkre:</p>

<p><a href="{{url('verify', $user->verification_token)}}">{{url('verify', $user->verification_token)}}</a></p>
