<h1>Kedves {{$user->name}}!</h1>

<p>Jelszavad visszaállításához kérjük kattints az alábbi linkre:</p>

<p><a href="{{ $link = route('password.reset', ['token' => $token, 'email' => $user->getEmailForPasswordReset()]) }}"> {{ $link }} </a></p>
