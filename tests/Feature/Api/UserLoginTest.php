<?php

namespace Tests\Feature\Api;

use App\Repositories\UserRepository;
use App\User;
use Cheppers\LaravelApiGenerator\Tests\Api\ApiTestBase;
use Illuminate\Http\Response;

class UserLoginTest extends ApiTestBase
{
    const PASSWORD = 123456;

    private function createUser($attributes = [])
    {
        return factory(User::class)->create($attributes);
    }

    public function testSuccessfulLogin()
    {
        $responseData = $this->createAndLoginActivatedUser();
        $this->assertArrayHasKey('access_token', $responseData);
        $this->assertArrayHasKey('token_type', $responseData);
        $this->assertArrayHasKey('expires_in', $responseData);
    }

    public function testFailedLogin()
    {
        $user = $this->createUser(
            [
                'email' => 'teszt@email.hu',
                'password' => UserRepository::hash(self::PASSWORD),
            ]
        );

        $response = $this->postJsonRequest(
            'api/user/login',
            [
                'data' => [
                    'type' => 'user',
                    'attributes' => [
                        'email' => $user->email,
                        'password' => md5('WrongPassword'),
                    ],
                ],
            ]
        );
        $responseData = $response->decodeResponseJson();
        $this->assertEquals('Helytelen email cím vagy jelszó', $responseData['errors'][0]['detail']);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testNotActivatedLogin()
    {
        $user = $this->createUser([
            'email' => 'teszt@email.hu',
            'password' => UserRepository::hash(self::PASSWORD),
        ]);

        $response = $this->postJsonRequest(
            'api/user/login',
            [
                'data' => [
                    'type' => 'user',
                    'attributes' => [
                        'email' => $user->email,
                        'password' => self::PASSWORD,
                    ],
                ],
            ]
        );

        $responseData = $response->decodeResponseJson();
        $this->assertEquals(
            'Kérjük, erősítse meg regisztrációját az email-ben kapott link segítségével!',
            $responseData['errors'][0]['detail']
        );
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
