<?php


namespace Tests\Feature\Api;

use App\Mail\Mailables\User\PasswordLinkSender;
use App\User;
use Cheppers\LaravelApiGenerator\Tests\Api\ApiTestBase;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

class UserForgotPasswordTest extends ApiTestBase
{

    /**
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function testUserEmailTriggered()
    {
        Mail::fake();
        $user = factory(User::class)->create([
            'email' => 'user@example.com',
        ]);
        $response = $this->postJsonRequest('api/user/forgotpassword', [
            'data' => [
                'type' => 'user',
                'attributes' => [
                    'email' => 'user@example.com',
                ],
            ],
        ]);
        Mail::assertQueued(
            PasswordLinkSender::class,
            function ($mailer) use ($user) {
                return $mailer->user->email === $user->email;
            }
        );

        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    public function testUserWithWrongEmail()
    {
        $response = $this->postJsonRequest('api/user/forgotpassword', [
            'data' => [
                'type' => 'user',
                'attributes' => [
                    'email' => 'wrong formatted email',
                ],
            ],
        ]);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(
            "The data.attributes.email must be a valid email address.",
            $responseData['errors'][0]['detail']
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUserWithMissingEmail()
    {
        $response = $this->postJsonRequest('api/user/forgotpassword', [
            'data' => [
                'type' => 'user',
                'attributes' => [],
            ],
        ]);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(
            "The data.attributes.email field is required.",
            $responseData['errors'][0]['detail']
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
