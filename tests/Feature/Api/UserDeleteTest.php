<?php

namespace Tests\Feature\Api;

use App\User;
use Carbon\Carbon;
use Cheppers\LaravelApiGenerator\Tests\Api\ApiTestBase;
use Illuminate\Http\Response;

class UserDeleteTest extends ApiTestBase
{
    private function createUser($attributes = [])
    {
        return factory(User::class)->create($attributes);
    }

    public function testDeleteUserForbidden()
    {
        $this->createUser([]);
        $response = $this->delete('api/user/remove');
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDeleteUser()
    {
        $user = $this->createUser([
            'email' => 'testuser@example.com',
            'password' => \Hash::make('123456'),
            'email_verified_at' => Carbon::now(),
        ]);
        $this->login('testuser@example.com', '123456');
        $response = $this->deleteJsonRequest('api/user/remove');
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertFalse(User::whereKey($user->id)->exists());
    }
}
