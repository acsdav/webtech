<?php
/**
 * Created by PhpStorm.
 * User: mozsi
 * Date: 2018.10.18.
 * Time: 15:19
 */

namespace Tests\Feature\Api;

use Cheppers\LaravelApiGenerator\Tests\Api\ApiTestBase;
use Illuminate\Http\Response;
use Spatie\Permission\Models\Permission;

class PermissionTest extends ApiTestBase
{
    public function testLoggedInUserAccessEndpointWithPermission()
    {
        $this->createAndLoginActivatedUser();
        Permission::create(['guard_name' => 'api', 'name' => 'testperm']);
        $this->user->givePermissionTo('testperm');
        $response = $this->getJsonRequest('api/test/endpointwithpermission');
        $response->assertStatus(Response::HTTP_OK);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1, $responseData['data']['id']);
        $this->assertEquals("pong", $responseData['data']['attributes']['ping']);
    }

    public function testLoggedInUserNotAccessEndpointWithOtherPermission()
    {
        $this->createAndLoginActivatedUser();
        Permission::create(['guard_name' => 'api', 'name' => 'otherperm']);
        $this->user->givePermissionTo('otherperm');
        $response = $this->getJsonRequest('api/test/endpointwithpermission');
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testUnauthenticatedUserNotAccessEndpoint()
    {
        $response = $this->getJsonRequest('api/test/endpointwithpermission');
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
