<?php

namespace Tests\Feature\Api;

use App\User;
use Carbon\Carbon;
use Cheppers\LaravelApiGenerator\Tests\Api\ApiTestBase;
use Illuminate\Http\Response;

class UserProfileTest extends ApiTestBase
{
    private function apiUserUpdate($attributes)
    {
        return $this->patchJsonRequest(
            'api/user/update-profile',
            [
                'data' => [
                    'type' => 'user',
                    'attributes' => $attributes,
                ]
            ]
        );
    }

    public function testUpdateValidUser()
    {
        $user = factory(User::class)->create([
            'name' => 'OldName',
            'email' => 'testuser@example.com',
            'password' => \Hash::make('123456'),
            'email_verified_at' => Carbon::now(),
        ]);
        $this->login('testuser@example.com', '123456');
        $response = $this->apiUserUpdate([
          'name' => 'UpdatedName',
        ]);
        $response->assertStatus(Response::HTTP_OK);

        $dbUser = User::find($user->id);
        $this->assertEquals('UpdatedName', $dbUser->name);
    }

    public function testUpdateNotAllowedFields()
    {
        factory(User::class)->create([
            'email' => 'testuser@example.com',
            'password' => \Hash::make('123456'),
            'email_verified_at' => Carbon::now(),
        ]);
        $this->login('testuser@example.com', '123456');
        $response = $this->apiUserUpdate([
          'email' => 'modified@example.com',
        ]);
        $response->assertStatus(Response::HTTP_OK);
        $responseData = $response->decodeResponseJson();

        $this->assertNotEquals('modified@example.com', $responseData['data']['attributes']['email']);
    }
}
