<?php
/**
 * Created by PhpStorm.
 * User: mozsi
 * Date: 2018.10.18.
 * Time: 13:32
 */

namespace Tests\Feature\Api;

use Cheppers\LaravelApiGenerator\Tests\Api\ApiTestBase;
use Illuminate\Http\Response;

class AuthTest extends ApiTestBase
{
    public function testUnauthenticatedUserAccessToUnauthPage()
    {
        $response = $this->getJsonRequest('api/test/unauthendpoint');
        $responseData = $response->decodeResponseJson();
        $response->assertStatus(Response::HTTP_OK);
        $this->assertEquals(1, $responseData['data']['id']);
        $this->assertEquals("pong", $responseData['data']['attributes']['ping']);
    }

    public function testLoggedInUserAccessToUnauthPage()
    {
        $this->createAndLoginActivatedUser();
        $response = $this->getJsonRequest('api/test/unauthendpoint');
        $responseData = $response->decodeResponseJson();
        $response->assertStatus(Response::HTTP_OK);
        $this->assertEquals(1, $responseData['data']['id']);
        $this->assertEquals("pong", $responseData['data']['attributes']['ping']);
    }
    public function testUnauthenticatedUserNotAccessToAuthPage()
    {
        $response = $this->getJsonRequest('api/test/authendpoint');
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testLoggedInUserAccessToAuthPage()
    {
        $this->createAndLoginActivatedUser();
        $response = $this->getJsonRequest('api/test/authendpoint');
        $responseData = $response->decodeResponseJson();
        $response->assertStatus(Response::HTTP_OK);
        $this->assertEquals(1, $responseData['data']['id']);
        $this->assertEquals("pong", $responseData['data']['attributes']['ping']);
    }
}
