<?php

namespace Tests\Feature\Api;

use App\Events\UserRegistered;
use App\User;
use Cheppers\LaravelApiGenerator\Tests\Api\ApiTestBase;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;

class UserRegisterTest extends ApiTestBase
{
    private $errorMessages = [
        'name_missing' => 'The data.attributes.name field is required.',
        'email_missing' => 'The data.attributes.email field is required.',
        'email_invalid' => 'The data.attributes.email must be a valid email address.',
        'password_missing' => 'The data.attributes.password field is required.',
        'password_mismatch' => 'The data.attributes.password confirmation does not match.',
        'password_too_short' => 'The data.attributes.password must be at least 6 characters.',
    ];

    private function createUser($name, $email, $password, $passwordConfirmation)
    {
        return $this->postJsonRequest(
            'api/user/register',
            [
                'data' => [
                    'type' => 'user',
                    'attributes' => [
                        'name' => $name,
                        'email' => $email,
                        'password' => $password,
                        'password_confirmation' => $passwordConfirmation,
                    ],
                ]
            ]
        );
    }

    public function validUserProvider()
    {
        return [
            ['someuser', 'someuser@some.host', 'somepw', 'somepw', Response::HTTP_CREATED],
        ];
    }

    public function invalidUserProvider()
    {
        return [
            [
                '',
                'someuser@some.host',
                'somepw',
                'somepw',
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $this->errorMessages['name_missing'],
            ],
            [
                'someuser',
                '',
                'somepw',
                'somepw',
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $this->errorMessages['email_missing'],
            ],
            [
                'someuser',
                'someuser@some.host',
                '',
                'somepw',
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $this->errorMessages['password_missing'],
            ],
            [
                'someuser',
                'someuser@some.host',
                'asd',
                'asd',
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $this->errorMessages['password_too_short'],
            ],
            [
                'someuser',
                'someuser@some.host',
                'somepw',
                '',
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $this->errorMessages['password_mismatch']],
            [
                'someuser',
                'someuser@some.host',
                'somepw',
                'differentpass',
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $this->errorMessages['password_mismatch'],
            ],
            [
                'someuser',
                'someuser@some.host',
                'differentpass',
                'somepw',
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $this->errorMessages['password_mismatch'],
            ],
            [
                'someuser',
                'someuser@',
                'somepw',
                'somepw',
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $this->errorMessages['email_invalid'],
            ],
        ];
    }

    /**
     * @dataProvider validUserProvider
     *
     * @param $name
     * @param $email
     * @param $password
     * @param $passwordConfirmation
     * @param $status
     */
    public function testCreateValidUser($name, $email, $password, $passwordConfirmation, $status)
    {
        Event::fake();
        $response = $this->createUser($name, $email, $password, $passwordConfirmation);
        $response->assertStatus($status);
        Event::assertDispatched(UserRegistered::class, function ($e) use ($email) {
            return $e->user->email === $email;
        });
    }

    /**
     * @dataProvider invalidUserProvider
     *
     * @param $name
     * @param $email
     * @param $password
     * @param $passwordConfirmation
     * @param $status
     * @param $errorMessage
     *
     * @throws \Exception
     */
    public function testCreateInvalidUser($name, $email, $password, $passwordConfirmation, $status, $errorMessage)
    {
        $response = $this->createUser($name, $email, $password, $passwordConfirmation);
        $response->assertStatus($status);
        $responseData = $response->decodeResponseJson();
        $this->assertSame($responseData['errors'][0]['detail'], $errorMessage);
    }

    public function testCreateUserWithSameEmail()
    {
        factory(User::class)->create(['email' => 'user@mail.com']);
        $response = $this->createUser('user23', 'user@mail.com', 'pass123', 'pass123');
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testCreateUserWithSameName()
    {
        $this->createUser('user', 'user1@mail.com', 'pass123', 'pass123');
        $response = $this->createUser('user', 'user2@mail.com', 'pass1233', 'pass1233');
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
