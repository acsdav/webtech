$(document).ready(function(){
    $(".submit-form-registration").on("click", function(e) {
        e.preventDefault();
        let data = getFormData($("form"));

        if (Object.size(data) >= 5) {
            let postRoute = $('.post-route').text();


            $.ajax({
                type: "POST",
                url: postRoute,
                async:false,
                dataType: "json",
                data: { data },
                success: function (data) {
                    setTimeout(function(){ window.location.href = "/login"; }, 2000);
                    $.notify("Siker, " + data['data'].attributes.name + "!", "success");
                },
                error: function (data) {
                    $.notify("Error", "error");
                    console.log('Error:', data);
                }
            });
        } else {
            $.notify("Error", "error");
        }
    });
});

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        if(n['value'].length>0) {
            indexed_array[n['name']] = n['value'];
        } else {
            return false;
        }
    });

    return indexed_array;
}
