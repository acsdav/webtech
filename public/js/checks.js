$(function(){

    if($('#status').val() == 0){
        $('#status').val('Aktív')
    }else if($('#status').val() == 1) {
        $('#status').val('Jóváhagyott')
    }else if($('#status').val() == -1){
        $('#status').val('Inaktív')
    }

    $("#change_link").on("click", function(e) {
        var from_amount = $("#from_amount").val();
        var from = $('#from_curr').val();
        var to = $('#to_curr').val();

        var temp = from_amount / from;
        var to_amount = temp * to;

        $('#to_amount').val(to_amount);
    });

});
