$(document).ready(function(){
    let deleteProfile = false

    if(!getCookie('isLogged') || getCookie('isLogged') === 'false') {
        // not logged in

        setCookie("isLogged", "false")
        $('.main-login-button').show();
        $('.main-registration-button').show();
        $('.main-logout-button').hide();
        $('.main-list-users-button').hide();
        $('.main-check-button').hide();
        $('.main-list-investments-button').hide();
        $('.main-profile-delete-button').hide();
        $('.main-profile-delete-button a').text("Profil törlése")

    } else {
        // LOgged in
        // WARNING!!!!!!!!!!!!!!!!!!!!!!!!
        // HA UJ PATH ADSZ HOZZÁ, IDE MINDENKÉPPEN VEDD FEL &&-ként
        if (window.location.pathname != '/' &&
            window.location.pathname != '/list-users' &&
            window.location.pathname != '/addcheck' &&
            window.location.pathname != '/checks' &&
            window.location.pathname != '/stornocheck' &&
            window.location.pathname != '/confirmcheck' &&
            window.location.pathname != '/modifycheck' &&
            window.location.pathname != '/list-investments' &&
            window.location.pathname != '/purchase' &&
            !window.location.pathname.startsWith('/checks') &&
            !window.location.pathname.startsWith('/exchange')
        ){
            window.location.href = '/'
        }
        $('.main-login-button').hide();
        $('.main-registration-button').hide();
        $('.main-logout-button').show();
        $('.main-list-users-button').show();
        $('.main-profile-delete-button').show();
        $('.main-check-button').show();
        $('.main-list-investments-button').show();
    }


    // kijeelntkezés
    $(".main-logout-button").on("click", function(e) {
        e.preventDefault();
        setCookie("userId", '')
        setCookie("isLogged", "false")
        window.location.href = '/'
    });


    // profil törlés
    $(".main-profile-delete-button").on("click", function(e) {
        e.preventDefault();
        if( deleteProfile === false) {
            deleteProfile = true;
            $('.main-profile-delete-button a').text("Biztos?")
        } else {
            deleteProfile = false
            userId = getCookie('userId')
            if(userId ) {
                let deleteRoute = $('.delete-route').text();
                console.log(deleteRoute)

                $.ajax({
                    type: "POST",
                    url: deleteRoute,
                    dataType: "json",
                    data: { "userId": userId },
                    success: function (data) {
                        console.log("sikeres törlés")
                        setCookie("userId", '')
                        setCookie("isLogged", "false")
                        window.location.href = '/'
                        $.notify("Sikeres törlés!", "success");
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $.notify("Error", "error");
                    }
                });
            }
            //window.location.href = '/'
        }
    });






    // Get a reference to the database service
    // var database = firebase.database();
    // database.collection("users").doc("wq8lJhbwHf5QWPy0").delete().then(function() {
    //     console.log("Document successfully deleted!");
    // }).catch(function(error) {
    //     console.error("Error removing document: ", error);
    // });

    $("#" + getCookie('userId')).css('background', 'rgba(174, 198, 207, 0.4)')
});


function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
