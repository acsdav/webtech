$(document).ready(function(){
    $(".submit-form-purchase").on("click", function(e) {
        e.preventDefault();
        let data = getFormData($("form"));
        quantity = Number(data.quantity);
        invID = data.invID;
        
        $.get("https://webtech-a85b1.firebaseio.com/inv_types.json", function(data){   
            for (var k in data){
                if(data[k] instanceof Object){
                    if (data[k].id == invID){
                        unit_price = data[k].unit_price;
                        total_price = quantity*unit_price;

                        $.get("https://webtech-f4aef.firebaseio.com/users.json", function(data){   
                            userId = getCookie('userId');
                            for (var k in data){
                                if(data[k] instanceof Object){
                                    if (data[k].id == userId){
                                        balance = data[k].balance;
                                        console.log(balance);

                                        if (total_price<=balance){
                                            $.notify("Van fedezet az egyenlegen.", "success");
                                        } else {
                                            $.notify("Túl kevés az egyenleg.", "error");
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

        if (Object.size(data) >= 3) {
            let postRoute = $('.post-route').text();
            userId = getCookie('userId');
            console.log(userId);
            $.extend(data,{'userID': userId});
            console.log(data);
        
        
            $.ajax({
                type: "POST",
                url: postRoute,
                async:false,
                dataType: "json",
                data: { data },
                success: function () {
                    $.notify("Sikeres vásárlás!", "success");
                },
                error: function (data) {
                    console.log('Error:', data);
                    $.notify("Error1", "error");
                }
            });
        } else {
            $.notify("Error", "error");
        }
    });
});



Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        if(n['value'].length>0) {
            indexed_array[n['name']] = n['value'];
        } else {
            return false;
        }
    });

    return indexed_array;
}
