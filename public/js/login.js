$(document).ready(function(){

    if(!getCookie('isLogged') || getCookie('isLogged') === 'false') {
        setCookie("isLogged", "false")
    } else {
        if (window.location.pathname != '/') {
            window.location.href = '/'
        }
    }

    $(".submit-form-login").on("click", function(e) {
        e.preventDefault();
            var username = $("#username").val();
            var password = $("#password").val();
            var valid = true
            if (password ==='') {
                $('input[type="password"]').css({"border": "1px solid red", "box-shadow": "0 0 2px red"});
                valid = false
            }
            if( username === '') {
                valid = false
                $('input[type="text"]').css({"border": "1px solid red", "box-shadow": "0 0 2px red"});
            }

            let data = getFormData($("form"));

        console.log(getCookie('isLogged'))
            if (getCookie('isLogged') && Object.size(data) >= 3 && valid) {
                let postRoute = $('.post-route').text();

                $.ajax({
                    type: "POST",
                    url: postRoute,
                    async:false,
                    dataType: "json",
                    data: { data },
                    success: function (data) {
                       setCookie("isLogged", "true")
                        setCookie("userId", data['id'])
                        window.location.href = '/'
                        $.notify("Sikeres login, " + data['name'] + "!", "success");
                    },
                    error: function (data) {
                        setCookie("isLogged", "false")
                        $.notify("Error", "error");
                        console.log('Error:', data);
                    }
                });
            } else {
                $.notify("Error", "error");
            }
    });
});

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        if(n['value'].length>0) {
            indexed_array[n['name']] = n['value'];
        } else {
            return false;
        }
    });

    return indexed_array;
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
