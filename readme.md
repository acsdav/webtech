# WebTechnológiák információs rendszerekben

3 BPMN diagram
logikai diagram
dataflow diagram (2 szinten)
projekt 

## Számlázás menedzselő oldal

#### Felhasználó management
 - A user a fenti BPMN diagramokban láható mezőkből tevődik össze
 - Userek számára külső végpont is fenáll, mellyel lehetőségük van regisztrálni
 - Továbbá a felhasználókezelés CRUD funkciói érhetőek el (Create, Read, Update, Delete)

#### Modul2
Modul2leirás

#### Modul3
Modul3leirás

## Projekt felállitása

- projekt klónozása
- .env.dist másolása és átnevezése .env
- másold docker-compose.override.yml.dist, docker-compose.override.yml -ra és szerkeszd ízlésed szerint
- futtatsd ezeket:
```
$ docker-compose up -d
$ A vagy B út
$ A: helpers/web_shell
web_shell$ composer install 
web_shell$ exit 
$ B: composer.phar install
$ helpers/artisan key:generate (opcionális)
$ helpers/artisan migrate (opcionális)
$ helpers/artisan jwt:secret (opcionális)
```

## Segítő scriptek
- helpers/web_shell - web container használata, root joggal
- helpers/artisan - laravel command managere

## Egyéb
- tests/postman/api.postman_collection.json - API route-ok látható gyűjteménye (teszteléshez remek)
