<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
  'prefix' => 'user'
], function ($router) {

    Route::post('remove', 'Api\UserController@remove')->name('api.auth.remove');

    Route::post('register', 'Api\UserController@register')->name('api.register');
    Route::post('login', 'Api\UserController@login')->name('api.auth.login');
    Route::post('logout', 'Api\UserController@logout')->name('api.auth.logout');
    Route::post('purchase', 'Api\PurchaseController@purchase')->name('api.purchase');
    //Route::post('forgotpassword', 'Api\UserController@forgotPassword')->name('api.forgotpassword');
    Route::group([
        'middleware' => 'jwtauth'
    ], function ($router) {
        //Route::get('me', 'Api\UserController@mydata')->name('api.auth.me');
        //Route::post('change-password', 'Api\UserController@changePassword');
        //Route::patch('update-profile', 'Api\UserController@updateProfile')->name('api.auth.update-profile');
        });
});

//
//Route::get('test/unauthendpoint', 'Api\TestController@testendpoint');
//Route::get('test/authendpoint', 'Api\TestController@testendpoint')->middleware(['jwtauth']);
//Route::get('test/endpointwithpermission', 'Api\TestController@testendpoint')
//    ->middleware(['jwtauth', 'permission:testperm']);
//
//Route::group([
//    'middleware' => ['jwtauth'],
//], function ($router) {
//    Route::apiresource('user', 'Api\UserController');
//});


