<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('/registration', function () {
    return view('registration');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/list-users', 'Controller@listUsers')->name('list.users');

Route::get('/exchange', 'CurrencyController@index');

Route::get('/load', 'CurrencyController@store');

Route::get('/update', 'CurrencyController@updateCurrency');

Route::get('/addcheck', function(){
    return view('addcheck');
});

Route::get('/stornocheck', function(){
   return view('stornocheck');
});

Route::get('/checks/{id}/confirmcheck', function($id){
    return view('confirmcheck')->with('id', $id);
});


Route::get('/checks/{id}/stornocheck', function($id){
    return view('stornocheck')->with('id', $id);
});

//Route::get('/checks/{id}/modifycheck', function($id) {
//    return view('modifycheck')->with('id', $id);
//});

Route::get('/checks/{id}/modifycheck', 'CurrencyController@getCurrencies');

//Route::GET('/check/{id}/modifycheck', 'CurrencyController@getCurrencies');

Route::resource('/checks', 'CheckController');

//
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//Route::get('elfelejtett-jelszo', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
//Route::post('elfelejtett-jelszo', 'Auth\ResetPasswordController@reset');
//Route::get('/verify/{token}', 'Auth\VerificationController@verify')->name('verify');

Route::get('/list-investments', 'InvestmentController@listInvestments');

Route::get('/purchase', function () {
    return view('purchase');
});
